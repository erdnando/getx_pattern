import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Store {
  static String TOKEN_GENERADO = '';


  static dialogo(String titulo, String mensaje){
    Get.dialog(AlertDialog(
        title: Text(titulo),
        content: Text(mensaje),
        actions: [
          FlatButton(
              onPressed: () {
                Get.back();
              },
              child: Text("OK"))
        ],
      ));
  }
}
