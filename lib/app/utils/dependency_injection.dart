import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/data/providers/local/local_auth.dart';
import 'package:getx_pattern/app/data/providers/remote/autentication_api.dart';
import 'package:getx_pattern/app/data/repositories/local/local_authentication_repository.dart';
import 'package:getx_pattern/app/data/repositories/remote/authentication_repository.dart';

//Carga las dependecias en memoria y las disponibiliza alos widgets en el arbol
//Aqui se puede implementar las unit test, creando mocks
class DependencyInjection {
  static void init() {
    Get.lazyPut<Dio>(() => Dio(BaseOptions(baseUrl: 'https://api.themoviedb.org/3')));
    Get.lazyPut<FlutterSecureStorage>(() => FlutterSecureStorage());
    
    //Providers
    Get.lazyPut<AuthenticationApi>(() => AuthenticationApi());
    Get.lazyPut<LocalAuth>(() => LocalAuth());
    
    //Repositories
    Get.lazyPut<AuthenticationRepository>(() => AuthenticationRepository());
    Get.lazyPut<LocalAuthRepository>(() => LocalAuthRepository());
    
  }
}
