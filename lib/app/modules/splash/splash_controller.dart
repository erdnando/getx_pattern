import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:getx_pattern/app/data/models/request_token.dart';
import 'package:getx_pattern/app/data/repositories/local/local_authentication_repository.dart';
import 'package:getx_pattern/app/data/repositories/remote/authentication_repository.dart';
import 'package:getx_pattern/app/routes/app_routes.dart';
import 'package:getx_pattern/app/utils/store.dart';

class SplashController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  _init() async {
    try {
      // RequestToken requestToken = await _repository.newRequestToken();
      // Store.TOKEN_GENERADO = requestToken.requestToken;

      //await Future.delayed(Duration(seconds: 5));//como un setTimeOut
      final RequestToken requestToken = await _repository.session;
   

      print('Token generado');
      print(Store.TOKEN_GENERADO);
      //redirije a una pagina
      Get.offNamed(requestToken!= null ? AppRoutes.HOME : AppRoutes.LOGIN);
    } catch (e) {
      print(e);
    }
  }
}
