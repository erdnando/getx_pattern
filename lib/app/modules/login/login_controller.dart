import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/data/models/request_token.dart';
import 'package:getx_pattern/app/data/repositories/remote/authentication_repository.dart';
import 'package:getx_pattern/app/utils/constants.dart';
import 'package:getx_pattern/app/utils/store.dart';

class LoginController extends GetxController {
  //inyeccion de dependencias
  final AuthenticationRepository _repository =
      Get.find<AuthenticationRepository>();

  String _username = '', _password = '';

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  _init() async {
    //logica inicial
    try {
      print("login controlle al inicio");
    } catch (e) {
      print(e);
    }
  }

  printMessage() {
    print("login controller al click");
  }

  void onUsernameChanged(String text) {
    _username = text;
  }

  void onPasswordChanged(String text) {
    _password = text;
  }

  Future<void> submit() async {
    try {
       RequestToken reqToken = await _repository.newRequestToken();
      Store.TOKEN_GENERADO = reqToken.requestToken;


      final RequestToken requestToken = await _repository.validateWithLogin(
          username: _username,
          password: _password,
          requestToken: Store.TOKEN_GENERADO);

      print('Login success...${requestToken.expiresAt}');
      Store.dialogo("SUCCESS", "Login successfull!!!!");
    } catch (e) {
      print(e);
      String message = "";
      if (e is DioError) {
        if (e.response != null) {
          message = e.response.statusMessage;
        } else {
          message = e.message;
        }
      }

      Store.dialogo("ERROR", message);
    }
  }
}
