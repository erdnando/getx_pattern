
import 'package:get/get.dart';


class HomeController extends GetxController {
  //inyeccion de dependencias


  @override
  void onReady() {
    super.onReady();
    _init();
  }

  _init() async {
    //logica inicial
    try {
      print("home controller al inicio");
    } catch (e) {
      print(e);
    }
  }

  printMessage() {
    print("home controller al click");
  }

  
}
