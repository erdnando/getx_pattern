import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:getx_pattern/app/modules/home/home_controller.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) {
        return Scaffold(
          body: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.transparent,
              padding: EdgeInsets.all(20),
            ),
        );
      },
    );
  }
}
