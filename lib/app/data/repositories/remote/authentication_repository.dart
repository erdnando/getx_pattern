import 'package:get/utils.dart';
import 'package:getx_pattern/app/data/models/request_token.dart';
import 'package:getx_pattern/app/data/providers/remote/autentication_api.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart' show required;

class AuthenticationRepository {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final AuthenticationApi _api = Get.find<AuthenticationApi>();

  Future<RequestToken> newRequestToken() {
    return _api.newRequestToken();
  }

   Future<RequestToken> validateWithLogin({
    @required String username, 
    @required String password, 
    @required String requestToken}) {
    return _api.validateWithLogin(username: username,password: password,requestToken: requestToken);
  }
}
